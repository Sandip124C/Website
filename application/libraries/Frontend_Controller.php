<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_Controller extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('my_helper'); //my own helper

		$this->data['meta_title'] = "Maharaniganj Secondary School";
		$this->data['phone'] = '023-345637';
		$this->data['mail'] = 'maharaniganj@gmail.com';
		$this->data['address'] = 'Arjundhara-11, Jhapa';
		$this->data['vendor'] = 'Chaudharys Design';
	}
}
