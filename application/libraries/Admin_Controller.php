<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('my_helper'); //my own helper

			$this->load->library('session');
			$this->load->library('form_validation');

		$this->data['meta_title'] = "Maharaniganj Secondary School";
		$this->data['version'] = '2.1';
	}
}
