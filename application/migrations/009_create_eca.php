<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_eca extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'title' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'category' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'slug' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                                'unique' => TRUE,
                        ),
                        'description' => array(
                                'type' => 'text',
                        ),
                        'pic' => array(
                                'type' => 'VARCHAR',
                                'constraint'=> '255'
                        ),
                        'status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '10',
                        ),
                        'created' => array(
                              'type' =>'DATETIME',
                        ),
                        'modified' => array(
                              'type' =>'DATETIME',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('eca');
        }

        public function down()
        {
                $this->dbforge->drop_table('eca');
        }
}
