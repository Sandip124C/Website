<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_rules extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                  'id' => array(
                          'type' => 'INT',
                          'constraint' => 11,
                          'unsigned' => TRUE,
                          'auto_increment' => TRUE
                  ),
                  'content' => array(
                          'type' => 'text',
                  ),
                  'created' => array(
                          'type' => 'timestamp',
                  ),
                  'modified' => array(
                          'type' => 'timestamp',
                  ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('rules');
        }

        public function down()
        {
                $this->dbforge->drop_table('rules');
        }
}
