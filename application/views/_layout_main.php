<!-- doctype declaration -->
<?php echo doctype('html5');?>
<html>
<head>
	<title>
		<?php
			echo $meta_title;
			echo link_tag('img/Graduation Cap.png', 'shortcut icon', 'image/ico');
	 ?>
	</title>
	<?php
	 $meta = array(
	        array(
	                'name' => 'Content-type',
	                'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
	        ),
	        array(
	                'name' => 'viewport',
	                'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	        ),
	        array(
	                'name' => 'robots',
	                'content' => 'no-cache'
	        ),
	        array(
	                'name' => 'description',
	                'content' => 'Maharaniganj Secondary School'
	        ),
	        array(
	                'name' => 'keywords',
	                'content' => 'Maharaniganj, School, Education, Arjundhara'
	        ),
	        array(
	                'name' => 'robots',
	                'content' => 'no-cache'
	        )
	  );
	echo meta($meta);
	echo link_tag('assets/bootstrap/css/bootstrap.min.css');
	echo link_tag('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css');
	echo link_tag('css/font-awesome.css');
	echo link_tag('css/style.css');
	echo link_tag('css/responsive.css');
	?>
</head>
<body>
<?php
 $this->load->view('layouts/nav');
 $this->load->view($partialView);
 $this->load->view('layouts/footer');
?>
</div>
</div>
</div>
</body>
<?php
 echo script_tag('js/jquery-1.11.3.min.js');
 echo script_tag('js/bootstrap.min.js');
 echo script_tag('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js');
 echo script_tag('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js');
 echo script_tag('js/main.js');
 ?>
	<script>
			$(function () {
					$('#navbar li a').filter(function () { return this.href == location.href }).parent().addClass('active').siblings().removeClass('active')
					$('#navbar li a').click(function () {
							$(this).parent().addClass('active').siblings().removeClass('active')
					})
			});
	</script>
</html>
