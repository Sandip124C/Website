	<div class="row footer-sub">
		<div class="container footer-inner">
			<div class="col-md-4">
				<h4>
					<?php echo strtoupper($meta_title); ?>
				</h4>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
						ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
						libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus
				</p>
			</div>
			<div class="col-md-4">
				<h4>
					USEFUL LINKS
				</h4>
			<?php
				echo '<p>'.anchor('/', 'Home', 'title="Home"').'</p>';
				echo '<p>'.anchor('notice', 'Notice', 'title="Notice"').'</p>';
				echo '<p>'.anchor('/gallery', 'gallery', 'title="Gallery"').'</p>';
				echo '<p>'.anchor('/rules', 'Rules', 'title="Rules"').'</p>';
				echo '<p>'.anchor('/about', 'About', 'title="About"').'</p>';
				echo '<p>'.anchor('/contact', 'Contact', 'title="Contact"').'</p>';
			?>
			</div>
			<div class="col-md-4">
				<h4>
					CONTACT INFO
				</h4>
				<p>
					<a href="#">
						<i class="fa fa-map-marker"></i>&nbsp;<?php echo $address; ?></a>
				</p>
				<p>
					<a href="tel:<?php echo $phone; ?>">
						<i class="fa fa-phone"></i>&nbsp;<?php echo $phone; ?></a>
				</p>
				<p>
					<a href="mailto:<?php echo $mail; ?>">
						<i class="fa fa-envelope"></i>&nbsp;<?php echo $mail; ?> </a>
				</p>
			</div>
		</div>
		<div class="row footer-small">
			<p class="footer-small-text">&copy; <?php echo $meta_title.' | '.date('Y');  ?> | Powered by <?php echo anchor('http://www.'.$vendor.'.com',$vendor)?></p>
		</div>
	</div>
