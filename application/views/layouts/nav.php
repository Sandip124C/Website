	<nav id="header" class="navbar navbar-default">
		<div id="contact-info" class="row">
			<div class="container text-right">
				<h4 class="social-info">
					<i class="fa fa-phone"></i> Phone:&nbsp;<a style="    color:#fff;
    text-decoration:none;" href="tel:<?php echo $phone ;?>"><?php echo $phone ;?></a>

					<?php
echo anchor('https://www.facebook.com/maharaniganjSchool1123',img(array('src' =>'img/Facebook.png', 'class' => 'social-icons','alt' => 'social icons')));
echo anchor('https://www.twitter.com/maharaniganjSchool1123',img(array('src' =>'img/Twitter.png', 'class' => 'social-icons','alt' => 'social icons')));
echo anchor('mailto:maharaniganjschool@gmail.com',img(array('src' =>'img/Message.png', 'class' => 'social-icons','alt' => 'social icons')));
					 ?>
				</h4>
			</div>
		</div>
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				 aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a style="color:#3c78e7;" class="navbar-brand" id="logo_nav" href="/">
<?php
	echo img(array('src'=> 'img/Graduation Cap.png', 'class' => 'img-responsive icon-head','alt'=>'brand image', 'style'=> 'max-height:30px; margin-top:-5px; display:inline-block;'));
	 echo ' '.$meta_title;
?>
				</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
<?php
	$list = array(
	anchor('/', 'Home', 'title="Home"'),
	anchor('notice', 'Notice', 'title="Notice"'),
	anchor('/gallery', 'gallery', 'title="Gallery"'),
	anchor('/rules', 'Rules', 'title="Rules"'),
	anchor('/about', 'About', 'title="About"'),
	anchor('/contact', 'Contact', 'title="Contact"')
	);

	$attributes = array(
	        'class' => 'nav navbar-nav navbar-right',
	);
	echo ul($list, $attributes);
?>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
