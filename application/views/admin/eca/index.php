<div class="row">
<div class="col-md-6">
<div class="box box-primary">
<div class="box-header with-border">
        <h3 class="box-title"><?php echo $page; ?></h3><span class="pull-right label label-md label-primary">
        <?php if($ecas){echo count($ecas); }else{ echo 0;} ?>
        </span>
      </div>
<div class="box-body">
      <!-- Notice block start -->
          <div class="post">
            <div class="row ">
              <?php
              if(!empty($ecas)){
                foreach($ecas as $eca){ ?>
              <div class="col-sm-12">
                <div class="row">
                   <div class="col-sm-3">
                     <?php
                     if($eca->pic ==''){
                       echo img(array('src' => 'img/uploads/notice_img.jpg', 'class' =>'img-notice img-responsive', 'alt' => $eca->title));
                     }else{
                     echo img(array('src' => e($eca->pic), 'class' =>'img-notice img-responsive', 'alt' => e($eca->title)));}
                     ?>
                  </div>
           <div class="col-sm-9">
           <h3><a href="/admin/ECA/edit/<?php echo e($eca->slug); ?>"><?php echo ucfirst(e($eca->title)); ?></a></h3>
           <h5 class="label label-info" title="category"><?php echo e($eca->category); ?></h5>
           <h5 class="label label-default" title="date" ><?php echo date('l j, Y',strtotime(e($eca->created))); ?></h5>
           <?php echo  $eca->status ? "<h5 class='label label-success'>Activated</h5>" : "<h5 class='label label-danger'>Deactivated</h5>";?>
           <p><?php echo substr(e($eca->description),0,150).' ...' ?></p>
          </div>
                </div>
              </div>
          <?php }
        }else{ echo emptyState('img/empty_box.jpg','Empty ECA','Notice not available.'); }?>
          </div>

        </div>
      </div>
   </div>
</div>
<?php $this->load->view('admin/eca/'.$eca_form.''); ?>
  </div>
