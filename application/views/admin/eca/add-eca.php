<div class="col-md-6">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#add-eca" data-toggle="tab">Add ECA detail</a></li>
    </ul>
    <div class="tab-content">
      <!-- /.tab-pane -->
   <div class="active tab-pane" id="add-eca">
     <?php
     if(has_alert()):
       foreach(has_alert() as $type => $message): ?>
         <div class="alert alert-dismissible <?php echo $type; ?>">
           <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
           <?php echo $message; ?>
         </div>
       <?php
      endforeach;
     endif; ?>

     <?php if(!empty(validation_errors())):?>
         <div class="alert alert-dismissible alert-warning">
           <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
           <?php echo validation_errors(); ?>
         </div>
         <?php endif; ?>
  <?php echo form_open('','class="form-horizontal"'); ?>
          <div class="form-group">
            <label for="Title" class="col-sm-3 control-label ">Title</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="title" required>
            </div>
          </div>

      <div class="form-group">
        <label for="category" class="col-sm-3 control-label ">Category</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="category" required>
        </div>
      </div>

      <div class="form-group">
        <label for="Title" class="col-sm-3 control-label ">Slug</label>
        <div class="col-sm-9">
          <input type="text" class="form-control"  name="slug"  required>
        </div>
      </div>

       <div class="form-group">
    <!-- <label for="description" class="col-sm-3 control-label ">Description</label> -->
        <div class="col-sm-12">
          <textarea type="text" class="form-control"  name="description" id="my_textarea" rows="3"  required></textarea>
          </div>
      </div>

          <div class="form-group" >
            <label for="image" class="col-sm-3 control-label">Image</label>
            <div class="col-sm-9">
              <input type="url" class="form-control" name="pic" >
            </div>
          </div>
          <div class="form-group" >
            <label for="image" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
              <input type="radio" class="flat-green" name="status"  value="1" required > Activated
              <input type="radio" class="flat-green" name="status"  value="0" required > Deactivated
            </div>
          </div>
          <div class="form-group">
          <label class="col-sm-3 control-label" for="Button"></label>
          <div class="col-sm-9 ">
            <button type="submit" class="btn btn-md bg-purple btn-flat" >Add ECA</button>
          </div>
          </div>
                <?php echo form_close();?>
      <!-- /.tab-pane -->
    </div>
    </div>
    </div>
  </div>
