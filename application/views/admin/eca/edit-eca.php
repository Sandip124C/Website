<div class="col-md-6">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#edit-eca" data-toggle="tab">Edit ECA detail</a></li>
      <li class="pull-right"><a  href="/admin/eca" class="label label-md label-danger"><i class="fa fa-close fa-lg"></i></a></li>
    </ul>
    <div class="tab-content">
      <!-- /.tab-pane -->
   <div class="active tab-pane" id="edit-eca">

  <?php if(!empty($eca_detail)){
    foreach($eca_detail as $element){
    ?>
    <?php if(!empty(validation_errors())):?>
        <div class="alert alert-dismissible alert-warning">
          <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
          <?php echo validation_errors(); ?>
        </div>
        <?php endif; ?>
    <?php echo form_open('','class="form-horizontal"'); ?>
          <input type="hidden" name="id" value="<?php echo $element->id; ?>">
          <div class="form-group">
            <label for="Title" class="col-sm-3 control-label ">Title</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="title"  value ="<?php echo $element->title; ?>"  required>
            </div>
          </div>

      <div class="form-group">
        <label for="category" class="col-sm-3 control-label ">Category</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="category" value ="<?php echo $element->category; ?>" required>
        </div>
      </div>

      <div class="form-group">
        <label for="Title" class="col-sm-3 control-label ">Slug</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="slug" value ="<?php echo $element->slug; ?>" required>
        </div>
      </div>

           <div class="form-group">
        <!-- <label for="description" class="col-sm-3 control-label ">Description</label> -->
            <div class="col-sm-12">
              <textarea type="text" class="form-control" name="description" rows="3" id ="my_textarea" rows="3"  required><?php echo e($element->description); ?></textarea>
              </div>
          </div>

          <div class="form-group" >
            <label for="image" class="col-sm-3 control-label">Image</label>
            <div class="col-sm-9">
              <input type="url" class="form-control" name="pic"  value="<?php echo $element->pic; ?>">
            </div>
          </div>
          <div class="form-group" >
            <label for="image" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
              <?php if($element->status == 1){ ?>
              <input type="radio" class="flat-green" name="status"  value="1" required checked > Activated
              <input type="radio" class="flat-green" name="status"  value="0" required> Deactivated
          <?php }else{ ?>
            <input type="radio" class="flat-green" name="status"  value="1" required  > Activated
            <input type="radio" class="flat-green" name="status"  value="0" required checked > Deactivated
          <?php } ?>
          </div>
          </div>
        <div class="form-group">
        <label class="col-sm-3 control-label" for="Button"></label>
        <div class="col-sm-9 ">
          <button type="submit" class="btn btn-md bg-purple btn-flat" >Edit ECA</button>
          <a href="/admin/eca/delete/<?php echo $element->slug; ?>" class="btn btn-md bg-orange btn-flat"  onclick="return confirm('Are you sure want to delete?'); ">Delete</a>
        </div>
          </div>
          <?php echo form_close();?>
      <?php } }?>
      <!-- /.tab-pane -->
    </div>
    </div>
    </div>
  </div>
