 <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b>&nbsp;<?php echo $version; ?>
        </div>
        <strong>Copyright &copy;&nbsp; <?php echo date('Y').' '. anchor('http://chauhdarydesign.com','Chaudhary Design Studio').'. '; ?></strong> All rights reserved.
      </footer>
      