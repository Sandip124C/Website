  <aside class="main-sidebar">
    <section class="sidebar">
 <div class="user-panel" style="background: url('../assets/dist/img/photo1.png') no-repeat; background-position: top; background-size: cover; ">
        <div class="pull-left image" style="padding:10px;">
      <img src="<?php echo site_url('/assets/dist/img/user2-160x160.jpg'); ?>" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
          <p></p>
          <a href="profile"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu">
        <?php
        echo '<li>'.anchor('admin/notice?action=add&amp;slug=', '<i class="fa fa-clipboard"></i><span>Notice</span>').'</li>';
        echo '<li>'.anchor('admin/slider?action=add&amp;slug=', '<i class="fa fa-arrow-right"></i><span>Slider</span>').'</li>';
        echo '<li>'.anchor('admin/gallery', '<i class="fa fa-image"></i><span>Gallery</span>').'</li>';
        echo '<li>'.anchor('admin/contact', '<i class="fa fa-phone"></i><span>Contact</span>').'</li>';
        echo '<li>'.anchor('admin/members', '<i class="fa fa-users"></i><span>Team Members</span>').'</li>';
        echo '<li>'.anchor('admin/rules', '<i class="fa fa-cubes"></i><span>Rules &amp; Regulation</span>').'</li>';
        echo '<li>'.anchor('admin/ECA', '<i class="fa fa-paw"></i><span>ECA</span>').'</li>';
        echo '<li>'.anchor('admin/studentVoice', '<i class="fa fa-microphone"></i><span>Student Voice</span>').'</li>';
        echo '<li>'.anchor('admin/principalMessage', '<i class="fa fa-envelope"></i><span>Principals Message</span>').'</li>';
        echo '<li>'.anchor('admin/setting', '<i class="fa fa-cog"></i><span>Setting</span>').'</li>';
        ?>
      </ul>
    </section>
  </aside>
