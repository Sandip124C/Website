<div class="col-md-5">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#edit-slider" data-toggle="tab">Edit Slider</a></li>
      <li class="pull-right"><a  href="/admin/slider?action=add&slug=" class="label label-md label-danger"><i class="fa fa-close fa-lg"></i></a></li>
    </ul>
    <div class="tab-content">
      <!-- /.tab-pane -->
   <div class="active tab-pane" id="edit-slider">
  <?php if(!empty($slider_detail)){
    foreach($slider_detail as $element){
    ?>
        <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
          <input type="hidden" name="id" value="<?php echo $element->id; ?>">
          <div class="form-group">
            <label for="Title" class="col-sm-3 control-label ">Title</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="title"  value ="<?php echo $element->title; ?>" required>
            </div>
          </div>

      <div class="form-group">
        <label for="category" class="col-sm-3 control-label ">Category</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="category" value ="<?php echo $element->category; ?>" required>
        </div>
      </div>

      <div class="form-group">
        <label for="Title" class="col-sm-3 control-label ">Slug</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="slug" value ="<?php echo $element->slug; ?>" required>
        </div>
      </div>

           <div class="form-group">
        <label for="description" class="col-sm-3 control-label ">Description</label>
            <div class="col-sm-9">
              <textarea type="text" class="form-control" name="description" rows="3" required>
                <?php echo $element->description; ?>
              </textarea>
              </div>
          </div>

          <div class="form-group" >
            <label for="image" class="col-sm-3 control-label">Image</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="pic" value="<?php echo $element->pic; ?>" required>
            </div>
          </div>
          <div class="form-group" >
            <label for="image" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
              <?php if($element->status == 1){ ?>
              <input type="radio" name="status"  value="1" required checked >Activated
              <input type="radio" name="status"  value="0" required> Deactivated
          <?php }else{ ?>
            <input type="radio" name="status"  value="1" required  >Activated
            <input type="radio" name="status"  value="0" required checked >Deactivated
          <?php } ?>
          </div>
          </div>
<div class="form-group">
<label class="col-sm-3 control-label" for="Button"></label>
<div class="col-sm-9 ">
  <button type="submit" class="btn btn-md btn-info btn-flat" >Edit Slider</button>
  <a href="/admin/slider?action=delete&slug=<?php echo $element->slug; ?>" class="btn btn-md btn-warning btn-flat"  onclick="return confirm("Are you sure want to delete?"); ">Delete</a>
</div>
          </div>
        </form>
      <?php } }?>
      <!-- /.tab-pane -->
    </div>
    </div>
    </div>
  </div>
