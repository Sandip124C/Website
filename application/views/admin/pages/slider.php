<section class="content">
  <div class="row">
 <div class="col-md-7">
 <div class="box box-primary">
 <div class="box-header with-border">
          <h3 class="box-title"><?php echo $page; ?></h3><span class="pull-right label label-md label-primary">
            <?php if(!empty($sliders)){
              echo count($sliders);
            }else{
              echo 0;
            } ?>
          </span>
        </div>
 <div class="box-body">
        <!-- Notice block start -->
            <div class="post">
              <div class="row ">
                <?php
                if(!empty($sliders)){
                  foreach($sliders as $slider){ ?>
                <div class="col-sm-12">
                  <div class="row">
                     <div class="col-sm-6">
                       <?php echo img(array('src' => $slider->pic, 'class' =>'img-notice img-responsive', 'alt' => $slider->title)); ?>
                         <img src="" class="img-thumbnail" />
                    </div>
                     <div class="col-sm-6">
                     <h3><a href="slider?action=edit&amp;slug=<?php echo $slider->slug; ?>"><?php echo $slider->title; ?></a></h3>
                     <h5 class="label label-info"><?php echo $slider->category; ?></h5>
                     <h5 class="label label-default"><?php echo date('l j, Y',strtotime($slider->created_at)); ?></h5>
                     <?php if($slider->status == 1){echo '<h5 class="label label-success">Activated</h5>';}else{ echo '<h5 class="label label-danger">Deactivated</h5>';} ?>
                     <p><?php echo substr($slider->description,0,150).' ...' ?></p>
                    </div>
                  </div>
                </div>

                  <?php }
                }else {    ?>
                  <!-- <p class="text-center text-warning"><i class="fa fa-warning fa-md"></i> Notice not available</p> -->
                  <div style="width:150px; margin:0px auto; padding:0px 10px; opacity:.5;">
                  <?php
                  echo img(array('src' => 'img/Empty_All.png', 'class' => 'img-responsive','alt' =>'Empty_note'));
                ?>
                <p class="text-muted">Sliders not available</p>
              </div>
              <?php }?>
              </div>
              </div>
              </div>

     </div>
  </div>
    <!-- /.col -->
<!-- form open -->
<?php $this->load->view('admin/pages/'.$slider_form.''); ?>
      <!-- form close -->
    </div>
  </div>
</section>
