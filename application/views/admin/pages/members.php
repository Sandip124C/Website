    <section class="content">
      <!-- Info boxes -->
      <div class="row ">
    <div class="col-sm-12 ">
        <a href="#" id="addMember"><button  class="btn btn-primary btn-md btn-flat"><i class="fa fa-user-md fa-lg"></i>&nbsp;Add Member</button></a>
        <a href="#" id="cancel"><button  class="btn btn-danger btn-md btn-flat"><i class="fa fa-close fa-lg"></i>&nbsp;Cancel</button></a>
        </div>
    </div><br>
     <div class=" row">
         <div class=" col-lg-12">
         <div class="box box-success" id="allMembers">
            <div class="box-header">
              <h3 class="box-title">All Team Member</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table  width="100%" class="table table-striped table-bordered table-hover" id="example1">
               <thead>
                <tr>
                  <th>ID</th>
                  <th>PICTURE</th>
                  <th>NAME</th>
                  <th>DESIGNATION</th>
                  <th>EMAIL</th>
                  <th>OPERATION</th>
                </tr>
                  </thead>
        <tbody>

<!-- start of member detail -->
           <tr>
         <td>{{ $team->id }}</td>
         <td><img src="{{ $team->profile_pic }}" class="img-circle" alt="User Image" width="50px" height="auto"></td>
         <td>{{ $team->name }}</td>
         <td>{{ $team->designation }}</td>
         <td>{{ $team->email }}</td>
    <td><a href='{{url("/setting/team/$team->id")}}' data-toggle="tooltip"  title='Edit'><button class='btn btn-md btn-success btn-flat'><i class="fa fa-pencil"></i></button></a>&nbsp;
    <a href='{{url("/setting/team/$team->id/delete")}}' data-toggle="tooltip"  title='Delete' ><button class='btn btn-md btn-danger btn-flat'><i class="fa fa-trash fa-lg"></i></button></a></td>
         </tr>
<!-- member detail  -->
          </tbody>
        </table>
        </div>
      </div>
     <div class="box box-success" id="addMemberForm">
     <div class="box-header with-border">
              <h3 class="box-title">Add Member Detail</h3>
            </div>
     <div class="box-body">
<form action="{{url('/setting/team/add')}}" method="post" class="form-horizontal" enctype="multipart/form-data" >

       <div class="alert alert-danger alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     <p><i class="fa fa-warning"></i> error</p>
     </div>

    <div class="form-group">
      <label class="col-sm-4 control-label" for="Name">Name</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" maxlength="30" name="name" required placeholder="name" />
        </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label" for="Email">Email</label>
        <div class="col-sm-6">
          <input type="email" class="form-control" maxlength="50" name="email"  required placeholder="email" />
        </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label" for="designation">Designation</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" maxlength="30" name="designation" required placeholder="designation" />
        </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label" for="Phone">Phone</label>
        <div class="col-sm-6">
          <input type="text" class=" form-control" name="phone" maxlength="15" required placeholder="phone" data-inputmask='"mask": "(999) 999-9999"' data-mask />
        </div>
    </div>

    <div class="form-group">
      <label class="col-sm-4 control-label" for="Facebook">Facebook</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" maxlength="50" name="facebook" required placeholder="Facebook" />
        </div>
    </div>
        <div class="form-group">
      <label class="col-sm-4 control-label" for="twitter">Twitter</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" maxlength="50" name="twitter" required placeholder="twitter" />
        </div>
    </div>
      <div class="form-group">
      <label class="col-sm-4 control-label" for="googleplus">Googleplus</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" maxlength="50" name="googleplus" required placeholder="googleplus" />
        </div>
    </div>
      <div class="form-group">
      <label class="col-sm-4 control-label" for="profile_pic">Profile Pic</label>
        <div class="col-sm-6">
          <input type="file" class="form-control" maxlength="20" name="profile_pic" required placeholder="Profile Pic" />
        </div>
    </div>
    <div class="form-group">
    <label class="col-sm-4 control-label" for="Button"></label>
        <div class="col-sm-6 ">
          <button type="submit" class="btn btn-md btn-info btn-flat" >Add Member Detail</button>
        </div>
</div>
</form>
</div>
</div>
     </div>
   </div>
   </section>
   </div>
 <script src="{{ Asset('vendor/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ Asset('vendor/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });

 $(document).ready(function () {
$("#addMemberForm").hide();
$("#cancel").hide();

    $("#addMember,#cancel").click(function(){
      $("#addMemberForm").toggle();
       $("#allMembers").toggle();
      $("#cancel").toggle();
       $("#addMember").toggle();
      });
  });
  </script>
  <script src="{{ Asset('vendor/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script type="text/javascript">
  $("[data-mask]").inputmask();
</script>

<script type="text/javascript" src="{{ Asset('vendor/plugins/gritter/js/jquery.gritter.js')}}"></script>
