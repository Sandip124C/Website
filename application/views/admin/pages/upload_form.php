<section class="content">
  <div class="row">
 <div class="col-md-7">
 <div class="box box-success">
 <div class="box-header with-border">
          <h3 class="box-title"><?php echo $page; ?></h3><span class="pull-right label label-md label-success"></span>
        </div>
 <div class="box-body">
        <!-- Notice block start -->
            <div class="post">
              <div class="row ">
                <?php
                echo $images;
                 ?>
              </div>
              </div>
              </div>

     </div>
  </div>
    <!-- /.col -->
    <div class="col-md-5">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#img-manager" data-toggle="tab">Upload Image</a></li>
        </ul>
        <div class="tab-content">
       <div class="active tab-pane" id="img-manager">
            <form class="form-horizontal" method="POST" action="/upload/do_upload" enctype="multipart/form-data">
              <div class="form-group" >
                <label for="image" class="col-sm-3 control-label">Image</label>
                <div class="col-sm-9">
                  <input type="file" class="form-control" name="userfile" required>
                </div>
              </div>
<div class="form-group">
<label class="col-sm-3 control-label" for="Button"></label>
    <div class="col-sm-9 ">
      <button type="submit" class="btn btn-md btn-info btn-flat" >Upload</button>
    </div>
              </div>
            </form>
          <!-- /.tab-pane -->
        </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
#copy-button {
  cursor: pointer;
  border: 0;
}

.clipboard {
  opacity: .8;
  background: none;

  color: rgba(255, 255, 255, 0.1);
  box-shadow: inset 0 0 0 2px rgba(255, 255, 255, 0.1);

  border-radius: 3px;

  outline: none !important;
}

  .clipboard:hover {
    box-shadow: inset 0 0 0 2px rgba(255, 255, 255, 0.3);
    color: rgba(255, 255, 255, 0.3);
  }
  .tooltipped {
  position: relative
}
.tooltipped:after {
  position: absolute;
  z-index: 1000000;
  display: none;
  padding: 5px 8px;
  color: #fff;
  text-align: center;
  text-decoration: none;
  text-shadow: none;
  text-transform: none;
  letter-spacing: normal;
  word-wrap: break-word;
  white-space: pre;
  pointer-events: none;
  content: attr(aria-label);
  background: rgba(0, 0, 0, 0.8);
  border-radius: 3px;
  -webkit-font-smoothing: subpixel-antialiased
}
.tooltipped:before {
  position: absolute;
  z-index: 1000001;
  display: none;
  width: 0;
  height: 0;
  color: rgba(0, 0, 0, 0.8);
  pointer-events: none;
  content: "";
  border: 5px solid transparent
}
.tooltipped:hover:before,
.tooltipped:hover:after,
.tooltipped:active:before,
.tooltipped:active:after,
.tooltipped:focus:before,
.tooltipped:focus:after {
  display: inline-block;
  text-decoration: none
}
.tooltipped-s:after {
  top: 100%;
  right: 50%;
  margin-top: 5px
}
.tooltipped-s:before {
  top: auto;
  right: 50%;
  bottom: -5px;
  margin-right: -5px;
  border-bottom-color: rgba(0, 0, 0, 0.8)
}

  </style>
<?php
echo script_tag('/js/clipboard.min.js');
 ?>
<script>
  var clipboard = new Clipboard('.clipboard');
  var showTooltip = function(elem, msg) {
 elem.setAttribute('class', 'clipboard tooltipped tooltipped-s');
 elem.setAttribute('aria-label', msg);
};

clipboard.on('success', function(e) {
   e.clearSelection();
   showTooltip(e.trigger, 'Copied!');
});

clipboard.on('error', function(e) {
   showTooltip(e.trigger, fallbackMessage(e.action));
});

var btn = document.querySelector('.clipboard');

btn.addEventListener('mouseleave', function(e) {
  e.currentTarget.setAttribute('class', 'clipboard');
  e.currentTarget.removeAttribute('aria-label');
});


</script>
