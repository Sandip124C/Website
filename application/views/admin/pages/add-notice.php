<div class="col-md-5">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#add-notice" data-toggle="tab">Add Notice</a></li>
    </ul>
    <div class="tab-content">
      <!-- /.tab-pane -->
   <div class="active tab-pane" id="add-notice">
     <?php if ($this->session->flashdata('response')) { ?>
  <div role="alert" class="alert alert-success">
     <button data-dismiss="alert" class="close" type="button">
       <span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
     <?php echo $this->session->flashdata('response') ?>
  </div>
  <?php } ?>
        <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
          <div class="form-group">
            <label for="Title" class="col-sm-3 control-label ">Title</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="title" required>
            </div>
          </div>

      <div class="form-group">
        <label for="category" class="col-sm-3 control-label ">Category</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="category" required>
        </div>
      </div>

      <div class="form-group">
        <label for="Title" class="col-sm-3 control-label ">Slug</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" name="slug" required>
        </div>
      </div>

           <div class="form-group">
        <label for="description" class="col-sm-3 control-label ">Description</label>
            <div class="col-sm-9">
              <textarea type="text" class="form-control" name="description" rows="3" required>
              </textarea>
              </div>
          </div>

          <div class="form-group" >
            <label for="image" class="col-sm-3 control-label">Image</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="pic" required>
            </div>
          </div>
          <div class="form-group" >
            <label for="image" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
              <input type="radio" class="" name="status"  value="1" required>Activated
              <input type="radio" class="" name="status"  value="0" required>Deactivated
            </div>
          </div>
<div class="form-group">
<label class="col-sm-3 control-label" for="Button"></label>
<div class="col-sm-9 ">
  <button type="submit" class="btn btn-md btn-info btn-flat" >Add Notice</button>
</div>
          </div>
        </form>
      <!-- /.tab-pane -->
    </div>
    </div>
    </div>
  </div>
