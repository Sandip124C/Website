    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
          @if(!empty($siteinfo))
          <img class="img-responsive " src="{{ $siteinfo->logo }}" alt="site logo">
          <h3 class="profile-username text-center">{{ $siteinfo->name }}</h3>
          @else
           <img class="img-responsive text-center" src="{{ url('../images/default.png') }}" alt="site logo">
          <h3 class="profile-username text-center">Site Name</h3>
          @endif
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">523</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
             <div class="col-lg-9">
     <div class="box box-success">
     <div class="box-header with-border">
<h3 class="box-title">Edit Site Information</h3>

     <div class="box-body">
         <!-- Site Info edit form -->
    <form class="form-horizontal" method="POST" action="/admin/siteinfo/{{ $siteinfo->id}}/edit" enctype="multipart/form-data">
         <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
   <label for="inputName" class="col-sm-2 control-label ">Name</label>
       <div class="col-sm-10">
      <input type="text" class="form-control" name="name" value="{{ $siteinfo->name}}" required>
          </div>
      </div>
    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="Email">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" maxlength="50" name="email"  required value="{{ $siteinfo->email}}" />
        </div>
    </div>
    <div class="form-group {{ $errors->has('location') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="location">Location</label>
        <div class="col-sm-10">
          <input type="text" class=" form-control" name="location" maxlength="20" required value="{{ $siteinfo->location}}" />
        </div>
    </div>
    <div class="form-group {{ $errors->has('facebook') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="Facebook">Facebook</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="50" name="facebook" value="{{ $siteinfo->facebook}}" required />
        </div>
    </div>
        <div class="form-group {{ $errors->has('twitter') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="twitter">Twitter</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="50" name="twitter"  value="{{ $siteinfo->twitter}}" required />
        </div>
    </div>
      <div class="form-group {{ $errors->has('googleplus') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="googleplus">Googleplus</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="50" name="googleplus"  value="{{ $siteinfo->googleplus}}" required />
        </div>
    </div>
       <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
          <label for="logo" class="col-sm-2 control-label">Logo</label>
         <div class="col-sm-10">
          <input type="file" class="form-control" name="logo" required>
        </div>
      </div>
    <div class="form-group">
    <label class="col-sm-2 control-label" for="Button"></label>
        <div class="col-sm-10 ">
       <button type="submit" class="btn btn-md btn-info btn-flat" >Edit Site Info</button>
        </div>
      </div>
     </form>
    @else
    <!-- Site Info add form -->
       <form class="form-horizontal" method="POST" action="{{ url('/admin/siteinfo/add') }}" enctype="multipart/form-data">
       <div class="alert alert-danger alert-dismissible">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
     <p><i class="fa fa-warning"></i> error</p>
     </div>
         <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
   <label for="inputName" class="col-sm-2 control-label ">Name</label>
       <div class="col-sm-10">
       <input type="text" class="form-control" name="name" placeholder="name" required>
          </div>
      </div>
    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="Email">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" maxlength="50" name="email"  required placeholder="email" />
        </div>
    </div>
    <div class="form-group {{ $errors->has('location') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="location">Location</label>
        <div class="col-sm-10">
          <input type="text" class=" form-control" name="location" maxlength="20" required placeholder="location" />
        </div>
    </div>
    <div class="form-group {{ $errors->has('facebook') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="Facebook">Facebook</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="50" name="facebook"  placeholder="facebook" required />
        </div>
    </div>
        <div class="form-group {{ $errors->has('twitter') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="twitter">Twitter</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="50" name="twitter"  placeholder="twitter" required />
        </div>
    </div>
      <div class="form-group {{ $errors->has('googleplus') ? ' has-error' : '' }}">
      <label class="col-sm-2 control-label" for="googleplus">Googleplus</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="50" name="googleplus"  placeholder="googleplus" required />
        </div>
    </div>
       <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
          <label for="logo" class="col-sm-2 control-label">Logo</label>
         <div class="col-sm-10">
          <input type="file" class="form-control" name="logo" required>
        </div>
      </div>
    <div class="form-group">
    <label class="col-sm-2 control-label" for="Button"></label>
        <div class="col-sm-10 ">
       <button type="submit" class="btn btn-md btn-info btn-flat" >Add Site Info</button>
        </div>
      </div>
     </form>
    @endif
</div>
</div>
</div>
        </div>
    </section>
    <!-- /.content -->
  </div>
