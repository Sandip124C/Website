<section class="content">
  <div class="row">
 <div class="col-md-12">
 <div class="box box-primary">
 <div class="box-header with-border">
          <h3 class="box-title"><?php echo $page; ?></h3><span class="pull-right label label-md label-primary">
            0
          </span>
        </div>
 <div class="box-body">
        <!-- Notice block start -->
            <div class="post">
              <div class="row ">
                  <div style="width:190px; margin:0px auto; padding:0px 10px; opacity:.5;">
                  <?php
                  echo img(array('src' => 'img/empty_inbox.jpg', 'class' => 'img-responsive','alt' =>'Empty_note'));
                ?>
                <p class="text-muted">No any Messages yet.</p>
              </div>
              </div>
              </div>
              </div>

     </div>
  </div>
    </div>
  </div>
</section>
