<!-- doctype declaration -->
<?php echo doctype('html5');?>
<html>
  <head>
    <title>
      <?php
        echo $meta_title;
        echo link_tag($icon, 'shortcut icon', 'image/ico');
     ?>
    </title>
<?php
 $meta = array(
        array(
                'name' => 'Content-type',
                'content' => 'text/html; charset=utf-8', 'type' => 'equiv'
        ),
        array(
                'name' => 'viewport',
                'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
        ),
        array(
                'name' => 'robots',
                'content' => 'no-cache'
        ),
        array(
                'name' => 'description',
                'content' => 'Maharaniganj Secondary School'
        ),
        array(
                'name' => 'keywords',
                'content' => 'Maharaniganj, School, Education, Arjundhara'
        ),
        array(
                'name' => 'robots',
                'content' => 'no-cache'
        )
  );
echo meta($meta);
echo link_tag('assets/bootstrap/css/bootstrap.min.css');
// echo link_tag('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
// echo link_tag('http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css');
echo link_tag('assets/dist/css/AdminLTE.min.css');
echo link_tag('assets/dist/css/skins/_all-skins.min.css');
echo link_tag('assets/plugins/iCheck/flat/blue.css');
echo link_tag('assets/plugins/morris/morris.css');
echo link_tag('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');
echo link_tag('assets/plugins/datepicker/datepicker3.css');
echo link_tag('assets/plugins/daterangepicker/daterangepicker-bs3.css');
echo link_tag('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
echo link_tag('css/font-awesome.css');
?>
  </head>
  <body class="skin-purple">
    <div class="wrapper">

<?php
 $this->load->view('admin/layouts/nav');
 $this->load->view('admin/layouts/sidebar');
?>

<div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo $icon.' '.$page; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><?php echo $icon.' '.$page; ?></li>
      </ol>
    </section>
    <!-- partial view -->
<?php $this->load->view($partialView); ?>
<!-- partial view -->
</div>

     <?php $this->load->view('admin/layouts/footer'); ?>
     <!--footer  -->
    </div><!-- ./wrapper -->

<?php
 echo script_tag('assets/plugins/jQuery/jQuery-2.1.3.min.js');
 // echo script_tag('http://code.jquery.com/ui/1.11.2/jquery-ui.min.js');
 echo script_tag('/assets/bootstrap/js/bootstrap.min.js');
 // echo script_tag('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');
 echo script_tag('assets/dist/js/app.min.js');
 echo script_tag('assets/dist/js/pages/dashboard.js');
 echo script_tag('assets/dist/js/demo.js');
?>
    <script>
        $(function () {
            $('.sidebar-menu li a').filter(function () { return this.href == location.href }).parent().addClass('active').siblings().removeClass('active')
            $('.sidebar-menu li a').click(function () {
                $(this).parent().addClass('active').siblings().removeClass('active')
            })
        });
    </script>
  </body>
</html>
