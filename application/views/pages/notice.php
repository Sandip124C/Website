	<div class="row">
		<div class="container">
			<div class="col-sm-8 col-sm-8 content-wrapper">
				<div class="row">
						<div class="col-md-12 col-sm-12">
					<h1><img src="<?php echo site_url('img/Strategy Board.png')?>" class="img-responsive icon-head" alt="Strategy Board">&nbsp;Notice &amp; Events</h1>
					<hr>
				</div>
<!-- // notice item -->
					<div class="row notice_item">
					<div class="col-md-4">
					<img src="<?php echo site_url('img/uploads/notice_img.jpg'); ?>" class="notice_img img-responsive" alt="notice_image"/>
					</div>
					<div class="notice_content col-md-8">
					<small><i class="fa fa-clock-o"></i> 10 Jan,2018</small>
					<hr>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
						ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
						libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus
						tempus, libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit.
					</p>
					</div>
					</div>
<!-- // notice item --><!-- // notice item -->
					<div class="row notice_item">
					<div class="col-md-4">
					<img src="<?php echo site_url('img/uploads/notice_img.jpg'); ?>" class="notice_img img-responsive" alt="notice_image"/>
					</div>
					<div class="notice_content col-md-8">
					<small><i class="fa fa-clock-o"></i> 10 Jan,2018</small>
					<hr>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
						ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
						libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus
						tempus, libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit.
					</p>
					</div>
					</div>
<!-- // notice item --><!-- // notice item -->
					<div class="row notice_item">
					<div class="col-md-4">
					<img src="<?php echo site_url('img/uploads/notice_img.jpg'); ?>" class="notice_img img-responsive" alt="notice_image"/>
					</div>
					<div class="notice_content col-md-8">
					<small><i class="fa fa-clock-o"></i> 10 Jan,2018</small>
					<hr>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
						ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
						libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus
						tempus, libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit.
					</p>
					</div>
					</div>
<!-- // notice item --><!-- // notice item -->
					<div class="row notice_item">
					<div class="col-md-4">
					<img src="<?php echo site_url('img/uploads/notice_img.jpg'); ?>" class="notice_img img-responsive" alt="notice_image"/>
					</div>
					<div class="notice_content col-md-8">
					<small><i class="fa fa-clock-o"></i> 10 Jan,2018</small>
					<hr>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
						ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
						libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus
						tempus, libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit.
					</p>
					</div>
					</div>
<!-- // notice item -->

				</div>
			</div>

			<?php $this->load->view('/pages/partials/archive_block.php');?>
	</div>
</div>
