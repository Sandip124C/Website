<div id="myCarousel" class="carousel slide swiper-container" data-ride="carousel">
		<!-- Indicators -->
			<?php if(count($sliders) > 0){
				$ol= '<ol class="carousel-indicators">';
				for($i=0; $i<count($sliders); $i++){
					$ol .='<li data-target="#myCarousel" data-slide-to="'.$i.'"';
					if($i== 0){
						$ol .='class="active"';
					}
					$ol .= ' ></li>';
				}
				$ol .= '</ol>';
				echo $ol; ?>
		<div class="carousel-inner" role="listbox">
			<?php
				foreach($sliders as $slider){ ?>
			<div class="item
			<?php
			if($slider->id == 1)
			{
				echo 'active';
			}else{
				 echo '';
			} ?>">
				<?php echo img(array('src' => $slider->pic, 'class' => $slider->category,'alt' => $slider->title)); ?>
				<div class="container">
					<div class="carousel-caption">
						<h1><?php  echo $slider->title; ?></h1>
						<p><?php  echo $slider->description; ?></p>
					</div>
				</div>
			</div>
		<?php }	}?>
			<!-- <div class="item">
				<img class="second-slide" src="<?php //echo site_url('img/uploads/slider_2.jpg')?>" alt="Second slide">
				<div class="container">
					<div class="carousel-caption">
						<h1>Another example headline.</h1>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam
							id dolor id nibh ultricies vehicula ut id elit.</p>
					</div>
				</div>
			</div>
			<div class="item">
				<img class="third-slide" src="<?php //echo site_url('img/uploads/slider_3.jpg')?>" alt="Third slide">
				<div class="container">
					<div class="carousel-caption">
						<h1>One more for good measure.</h1>
						<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam
							id dolor id nibh ultricies vehicula ut id elit.</p>
					</div>
				</div>
			</div> -->
		</div>
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
	</div>
	<!-- /.carousel -->
	<div class="row" id="myJumbotron">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1>Welcome to
						<b class="text-uppercase"><?php echo $meta_title; ?></b>
					</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
						ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
						libero nunc consectetur tellus, ut maximus augue justo ut purus. Pellentesque fringilla justo sit amet euismod tempor.
					</p>
				</div>
				<hr>
			</div>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="container">
			<div class="col-sm-8 content-wrapper">
				<div class="row">
					<div class="container col-sm-12">
						<h1>
							<?php echo img(array('src'=> 'img/Administrator.png', 'class' => 'img-responsive icon-head','alt'=>'Administrator'));?> Principal's Message</h1>
						<hr>
						<p><?php echo img(array('src'=> 'img/uploads/principal.jpg', 'class' => 'img-responsive principal','alt'=>'Principal'));?> Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
							ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
							libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus
							tempus, libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit
							et faucibus tempus, libero nunc ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In
							ornare, velit et faucibus tempus, libero nunc consectetur tellus, ut maximus augue justo ut purus. Pellentesque fringilla
							justo sit amet euismod tempor.
							<blockquote> ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
								libero nunc
							</blockquote>
						</p>
						<hr>
					</div>
				</div>
			<div class="row">
				<div class="container col-sm-12">
					<h1>
						<?php echo img(array('src'=> 'img/Microphone.png', 'class' => 'img-responsive icon-head','alt'=>'Microphone'));?> Student Voice</h1>
					<hr>
					<div class="row">
						<div class="col-md-3">
							<?php echo img(array('src'=> 'img/uploads/user3.jpg', 'class' => 'img-circle student','alt'=>'student'));?>
						</div>
						<div class="col-md-9">
							<h3>Lorem Ipsum</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus
								augue nsectetur tellus, ut maximus augue justo ut purus. Pellentesque fringilla justo sit amet euismod tempor.
							</p>
						</div>
					</div>
					<hr>
				</div>
			</div>
				<div class="row">
				<div class="container col-sm-12">
					<h1>
							<?php echo img(array('src'=> 'img/Conference.png', 'class' => 'img-responsive icon-head','alt'=>'Conference'));?> Our Staffs</h1>
					<hr>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
						nsectetur tellus, ut maximus augue justo ut purus. Pellentesque fringilla justo sit amet euismod tempor.
					</p>
					<div class="row">
						<div class="col-md-3">
							<?php echo img(array('src'=> 'img/uploads/user1.jpg', 'class' => 'img-responsive staff'));?>
						</div>
						<div class="col-md-3">
							<?php echo img(array('src'=> 'img/uploads/user4.jpg', 'class' => 'img-responsive staff'));?>
						</div>
						<div class="col-md-3">
							<?php echo img(array('src'=> 'img/uploads/user3.jpg', 'class' => 'img-responsive staff'));?>
						</div>
						<div class="col-md-3">
							<?php echo img(array('src'=> 'img/uploads/user4.jpg', 'class' => 'img-responsive staff'));?>
						</div>
					</div>
					<hr>
				</div>
			</div>
				<div class="row">
				<div class="container col-sm-12">
					<h1>
						<?php echo img(array('src'=> 'img/telephone.png', 'class' => 'img-responsive icon-head','alt'=>'Phone'));?> Contact Us</h1>
					<hr>
					<p></p>
					<div class="row">
						<div class="col-md-4">
							<h5>
								<i class="fa fa-map-marker"></i>&nbsp;Arjundhara-11, Jhapa</h5>
						</div>
						<div class="col-md-4">
							<h5>
								<i class="fa fa-phone"></i>&nbsp;023-234566</h5>
						</div>
						<div class="col-md-4">
							<h5>
								<i class="fa fa-envelope"></i>&nbsp;maharaniganj@gmail.com</h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
						<iframe class="map-section" src="https://www.google-maps.com/gfdyl=2345jgfj345jsk0#hsdh287djj3HHGHE"></iframe>
					</div>
					</div>
					<hr>
				</div>
			</div>
		</div>
		<?php $this->load->view('/pages/partials/archive_block.php');?>
	</div>
</div>

<link rel="stylesheet" href="<?php echo site_url('css/swiper.min.css');?>">
<script src="<?php echo site_url('js/swiper.min.js');?>"></script>

<!-- Initialize Swiper -->
<script>
	var swiper = new Swiper('.swiper-container', {
		slidesPerView: 1,
		spaceBetween: 10,
		loop: true,
		autoplay: {
			delay: 2500,
			disableOnInteraction: false,
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
	});
</script>
