	<div class="row">
		<div class="container" >
			<div class="col-sm-8 col-sm-8 content-wrapper">
				<div class="row">
						<div class="col-md-12 col-sm-12">
					<h1>
							<?php echo img(array('src'=> 'img/about.png', 'class' => 'img-responsive icon-head','alt'=>'About Us'));?> About Us</h1>
					<hr>
				</div>
			</div>

					<div class="row col-md-12">
						<?php echo img(array('src' => 'img/uploads/about_banner.jpg', 'class' => 'img-responsive', 'alt' => 'about_banner')); ?>
					<br>
					</div>
					<div class="row notice_content col-md-12">
					<small> Shree Maharaniganj Secondary School</small>
					<hr>
					<p>Shree Maharaniganj Secondary School is situated at Arjundhara-11, Salbari, Jhapa Serving quality education. Just approx 2.5 km north from Mahendra Highway (Buttabari).
					</p>
					<hr>
					</div>
								<div class="row">
				<div class="container col-sm-12">
					<h1>
						<?php echo img(array('src' => 'img/Conference.png', 'class' => 'img-responsive icon-head', 'alt' => 'Our Staff')); ?> Our Staffs</h1>
					<hr>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
						nsectetur tellus, ut maximus augue justo ut purus. Pellentesque fringilla justo sit amet euismod tempor.
					</p>
					<div class="row">
						<link rel="stylesheet" href="<?php echo site_url('css/swiper.min.css');?>">

						  <!-- Demo styles -->
						  <style>
						    .swiper-container {
						      width: 100%;
						      height: 200px;
						    }
								.swiper-wrapper{
									margin:0px;
									padding:0px;
								}
						    .swiper-slide {
									margin:0px;
									padding:0px;
						      text-align: center;
						      font-size: 18px;
						      background: #fff;
									border:1px solid #eee;

						      /* Center slide text vertically */
						      display: -webkit-box;
						      display: -ms-flexbox;
						      display: -webkit-flex;
						      display: flex;
						      -webkit-box-pack: center;
						      -ms-flex-pack: center;
						      -webkit-justify-content: center;
						      justify-content: center;
						      -webkit-box-align: center;
						      -ms-flex-align: center;
						      -webkit-align-items: center;
						      align-items: center;
						    }
								.swiper-slide .swiper-img{
									margin:0px;
									padding:0px;
									background: red;

								}
								.slide-title{
									position:absolute;
									margin-top:0px;
									padding:5px 0px 0px 0px;
									top:160px;
									background: #3c78e7;
									color:#fff;
									width:100%;
									height:40px;
								}
						  </style>
						</head>
						  <!-- Swiper -->
							<div class="swiper-container">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<img class="swiper-img" src="<?php echo site_url('img/uploads/user1.jpg')?>" alt="First slide">
										<p class="slide-title">Sandip Chaudhary</p>
									</div>
									<div class="swiper-slide">
										   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">
										 	<p class="slide-title">Mangal Pandey</p>
										</div>
									<div class="swiper-slide">
										   <img class="swiper-img" src="<?php echo site_url('img/uploads/user1.jpg')?>" alt="First slide">
										 	<p class="slide-title">Albert Einstein</p>
										</div>
									<div class="swiper-slide">
										   <img class="swiper-img" src="<?php echo site_url('img/uploads/user4.jpg')?>" alt="First slide">
										 	<p class="slide-title">Nicolas Corpenicus</p>
										</div>
									<div class="swiper-slide">
										   <img class="swiper-img" src="<?php echo site_url('img/uploads/user1.jpg')?>" alt="First slide">
										 	<p class="slide-title">Prithivi Narayan Singh</p>
										</div>
									<div class="swiper-slide">
										   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">
										 	<p class="slide-title">Leonardo da vinci</p>
										</div>
								</div>
								<!-- Add Pagination -->
								<div class="swiper-pagination"></div>
							</div>


						  <!-- Swiper JS -->
						  <script src="<?php echo site_url('js/swiper.min.js');?>"></script>

						  <!-- Initialize Swiper -->
						  <script>
					      var swiper = new Swiper('.swiper-container', {
					        slidesPerView: 4,
					        spaceBetween: 10,
					        loop: true,
					        autoplay: {
					          delay: 2500,
					          disableOnInteraction: false,
					        },
					        pagination: {
					          el: '.swiper-pagination',
					          clickable: true,
					        },
					      });
						  </script>
					</div>
					<hr>
				</div>
			</div>
        </div>
				<?php $this->load->view('/pages/partials/archive_block.php');?>
	</div>
</div>
