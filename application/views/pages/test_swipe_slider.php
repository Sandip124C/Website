<link rel="stylesheet" href="<?php echo site_url('css/swiper.min.css');?>">
<style>
   .swiper-container {
     width: 100%;
     height:450px;
   }
   .swiper-slide {
     text-align: center;
     font-size: 18px;
     background: #fff;

     /* Center slide text vertically */
     display: -webkit-box;
     display: -ms-flexbox;
     display: -webkit-flex;
     display: flex;
     -webkit-box-pack: center;
     -ms-flex-pack: center;
     -webkit-justify-content: center;
     justify-content: center;
     -webkit-box-align: center;
     -ms-flex-align: center;
     -webkit-align-items: center;
     align-items: center;
   }
     .swiper-img{
       width:100%;
       height:auto;
       position: absolute;
   }
   .swiper-content{
     text-align:center;
     margin:0px auto;
     position:relative;
     z-index: 100;
     margin-top:350px;
     color:#eee;
     width:60%;
   }
   /* for large screens */
@media screen and (min-width:1280px){
   .swiper-container{
     max-height:450px;
   }
}
   /* for large screens */
@media screen and (min-width:1024px){
   .swiper-container{
     max-height:450px;
   }
        .swiper-content{
     margin-top:290px;
   }
}
/* for medium screen */
@media screen and (max-width:960px){
    .swiper-content{
       margin-top:350px;
   }
    .swiper-container{
     max-height:190px;
   }
}
/* for small screen */
@media screen and (max-width:480px){
   .swiper-container{
     max-height:100px;
     width:100%;
   }
   .swiper-content >h1{
      display:none;
   }
}
 </style>
<div class="swiper-container">
   <div class="swiper-wrapper">
     <div class="swiper-slide">
       <img class="swiper-img" src="<?php echo site_url('img/slider_1.jpg')?>" alt="First slide">
       <div class="swiper-content">
     <h1>Example headline.</h1>
 </div>
 </div>
 <div class="swiper-slide">
       <img class="swiper-img" src="<?php echo site_url('img/slider_2.jpg')?>" alt="First slide">
       <div class="swiper-content">
     <h1>Example headline.</h1>
 </div>
 </div>
 <div class="swiper-slide">
       <img class="swiper-img" src="<?php echo site_url('img/slider_3.jpg')?>" alt="First slide">
       <div class="swiper-content">
     <h1>Example headline.</h1>
 </div>
 </div>
   </div>
   <!-- Add Pagination -->
   <div class="swiper-pagination"></div>

   <div class="swiper-button-next"></div>
   <div class="swiper-button-prev"></div>

 </div>
  <script src="<?php echo site_url('js/swiper.min.js');?>"></script>

 <!-- Initialize Swiper -->
 <script>
   var swiper = new Swiper('.swiper-container', {
     pagination: {
       el: '.swiper-pagination',
       dynamicBullets: true,
     },
      navigation: {
       nextEl: '.swiper-button-next',
       prevEl: '.swiper-button-prev',
     },
   });
 </script>

   <div class="row" id="myJumbotron">
   <div class="container">
     <div class="row">
       <div class="col-sm-12">
         <h1>Welcome to
           <b class="text-uppercase"><?php echo $meta_title; ?></b>
         </h1>
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In bibendum, ipsum ac porttitor accumsan.Maecenas rhoncus augue
           ex, sit amet placerat sem dapibus eu. Phasellus tincidunt nec mi nec consequat. In ornare, velit et faucibus tempus,
           libero nunc consectetur tellus, ut maximus augue justo ut purus. Pellentesque fringilla justo sit amet euismod tempor.
         </p>
       </div>
       <hr>
     </div>
     <hr>
   </div>
 </div>
