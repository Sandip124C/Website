  <div class="row">
    <div class="container">
      <div class="col-sm-8 col-sm-8 content-wrapper">
        <div class="row">
            <div class="col-md-12 col-sm-12">
          <h1><img src="<?php echo site_url('img/rules.png')?>" class="img-responsive icon-head" alt="Rules">&nbsp;Rules</h1>
          <hr>
        </div>
        <div class="row">
        <div class="col-md-12">
          <link rel="stylesheet" href="<?php echo site_url('css/swiper.min.css');?>">

            <!-- Demo styles -->
            <style>
              .swiper-container {
                width: 100%;
                height: 250px;
              }
              .swiper-slide {
                text-align: center;
                font-size: 18px;
                background: #fff;

                /* Center slide text vertically */
                display: -webkit-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                -webkit-box-pack: center;
                -ms-flex-pack: center;
                -webkit-justify-content: center;
                justify-content: center;
                -webkit-box-align: center;
                -ms-flex-align: center;
                -webkit-align-items: center;
                align-items: center;
              }
            </style>
          </head>
            <!-- Swiper -->
            <div class="swiper-container">
              <div class="swiper-wrapper">
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user1.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user1.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user1.jpg')?>" alt="First slide">lorem ipsum</div>
            <div class="swiper-slide">   <img class="swiper-img" src="<?php echo site_url('img/uploads/user3.jpg')?>" alt="First slide">lorem ipsum</div>
              </div>
              <!-- Add Pagination -->
              <div class="swiper-pagination"></div>
            </div>

            <!-- Swiper JS -->
            <script src="<?php echo site_url('js/swiper.min.js');?>"></script>

            <!-- Initialize Swiper -->
            <script>
              var swiper = new Swiper('.swiper-container', {
                slidesPerView: 4,
                spaceBetween: 10,
                autoplay: {
                  delay: 2500,
                  disableOnInteraction: false,
                },
                pagination: {
                  el: '.swiper-pagination',
                  clickable: true,
                },
              });
            </script>
        </div>
        </div>
        </div>
      </div>
      <?php $this->load->view('/pages/partials/archive_block.php');?>
  </div>
</div>
