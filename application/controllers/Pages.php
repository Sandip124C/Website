<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Frontend_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('model_user');
		$this->load->model('model_notice');
		$this->load->model('model_slider');
	}

	public function index()
	{
		$this->data['partialView'] = 'pages/home';

		$this->data['sliders'] = $this->model_slider->get();

		$this->load->view('_layout_main',$this->data);
	}

	public function notice()
	{
		$this->data['partialView'] = 'pages/notice';
		$this->load->view('_layout_main',$this->data);
	}

	public function gallery()
	{
		$this->data['partialView'] = 'pages/gallery';
		$this->load->view('_layout_main',$this->data);
	}

	public function rules()
	{
		$this->data['partialView'] = 'pages/rules';
		$this->load->view('_layout_main',$this->data);
	}

	public function about()
	{
		$this->data['partialView'] = 'pages/about';
		$this->load->view('_layout_main',$this->data);
	}

	public function contact()
	{
		$this->data['partialView'] = 'pages/contact';
		$this->load->view('_layout_main',$this->data);
	}

	//
	public function swipe()
	{
		$this->data['partialView'] = 'pages/test_swipe_slider';
		$this->load->view('_layout_main',$this->data);
	}
	public function myform()
	{
		// $this->data['partialView'] = 'pages/test';
		// $this->load->view('pages/test');
		$this->form_validation->set_rules(
		        'username', 'Username',
		        'trim|required|min_length[5]|max_length[12]|is_unique[users.name]',
		        array(
		                'required'      => 'You have not provided %s.',
		                'is_unique'     => 'This %s already exists.'
		        )
		);
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');

		                if ($this->form_validation->run() == FALSE)
		                {
		                        $this->load->view('pages/myform');
		                }
		                else
		                {
		                        $this->load->view('pages/test_swipe_slider');
		                }
	}
	public function username_check($str)
{
				if ($str == 'test')
				{
								$this->form_validation->set_message('username_check', 'The {field} field can not be the word "test"');
								return FALSE;
				}
				else
				{
								return TRUE;
				}
}
}
