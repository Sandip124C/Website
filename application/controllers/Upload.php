<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends Admin_Controller {

        public function __construct()
        {
          parent::__construct();
          $this->load->helper(array('form', 'url'));
        }

        public function index()
        {
          $this->data['partialView'] = 'admin/pages/upload_form';
          $this->data['page'] = 'Image Manager';
      		$this->data['icon'] = '<i class="fa fa-image"></i>';

          $this->data['error'] = '';
          $this->data['images'] = "";
          $folder = "img/uploads/";

          $filesInFolder = new DirectoryIterator( $folder);
            while ( $filesInFolder->valid() ) {
            $file = $filesInFolder->current();
            $filename = $file->getFilename();
            $src = base_url()."$folder/$filename";
            if(!file_exists($src)){
              $this->data['images']= "<div style='width:150px; margin:0px auto; padding:0px 10px; opacity:.5'>".
              img(array('src' => 'img/empty_folder.jpg', 'class' => 'img-responsive','alt' =>'Empty_note')).
              "<p class='text-muted'>No images yet.</p></div>";
            }
            $fileInfo = new Finfo( FILEINFO_MIME_TYPE );
            $mimeType = $fileInfo->file( $folder.'/'.$filename );
              if ( $mimeType === 'image/jpeg' || $mimeType === 'image/jpg' || $mimeType === 'image/png' || $mimeType === 'image/gif' ) {
              $href = "?delete-image=$src";
              $this->data['images'] .= "<div class='col-md-3'><img class='img-responsive' src='$src' />
              <a class='clipboard' data-clipboard-text='$src'><i class='fa fa-copy fa-md text-green' title='Copy link'></i></a>
              <a href='$href'><i class='fa fa-trash fa-md text-red'></i></a></div>";
              }
            $filesInFolder->next();
            }
          $this->load->view('admin/_layout_main',$this->data);
        }

        public function do_upload()
        {
                $config['upload_path']          = './img/uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1000;
                $config['max_width']            = 2024;
                $config['max_height']           = 1068;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $this->data['error'] = array('error' => $this->upload->display_errors());
                }
                redirect('/upload');
        }
}
?>
