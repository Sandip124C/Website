<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_user');
		$this->load->model('model_notice');
		$this->load->model('model_slider');
	}

	//////////////////////////////////////////// index  function////////////////////////////////////////
	public function index()
	{
		$this->data['partialView'] = 'admin/pages/dashboard';
		$this->data['page'] = 'Dashboard';
		$this->data['icon'] = '<i class="fa fa-dashboard"></i>';
		$this->load->view('admin/_layout_main',$this->data);
	}

	public function login()
	{
		$this->load->view('admin/pages/login');
		//file not created
	}
	//////////////////////////////////////////// notice function////////////////////////////////////////
	public function notice()
	{
		$this->data['partialView'] = 'admin/pages/notice';
		$this->data['page'] = 'Notice';
		$this->data['icon'] = '<i class="fa fa-clipboard"></i>';

		$this->data['notices'] = $this->model_notice->get();

		if($_GET['action'] != NULL){
		$action = $_GET['action'];
		}else{
			$action = 'add';
		}
		if($_GET['slug'] != NULL){
		$slug = $_GET['slug'];
		}else{
			$slug = '';
		}
		if($action == 'edit'){
			$this->data['notice_form'] = 'edit-notice';
			if($slug != ''){
				$this->data['notice_detail'] = $this->model_notice->get_by(array('slug' => $slug));
			}
		}else{
			$this->data['notice_form'] = 'add-notice';
		}
		if(!empty($_POST) && $action == 'add')
		{
			$data =  array('title' => $this->input->post('title'),
					'category' => $this->input->post('category'),
					'slug' => url_title($this->input->post('slug')),
					'description' => $this->input->post('description'),
					'pic' => $this->input->post('pic'),
					'status' => $this->input->post('status')
				);
			$this->model_notice->save($data);
			$this->session->set_flashdata('response', "Notice Saved Successfully!!");
			redirect('admin/notice?action=add&slug=');
		}
		else if(!empty($_POST) && $action == 'edit' && $slug != '')
		{
			$data =  array('title' => $this->input->post('title'),
					'category' => $this->input->post('category'),
					'slug' => url_title($this->input->post('slug')),
					'description' => $this->input->post('description'),
					'pic' => $this->input->post('pic'),
					'status' => $this->input->post('status')
				);
				$parameter =$this->input->post('id');
			$this->model_notice->save($data,$parameter);
			$this->session->set_flashdata('response', "Notice updated Successfully!!");
			redirect('admin/notice?action=add&slug=');
		}		else if($action == 'delete' && $slug != '')
		{
			$this->model_notice->delete_by(array('slug' => $slug));
			$this->session->set_flashdata('response', "Notice deleted Successfully!!");
			redirect('admin/notice?action=add&slug=');

		}
		else
		{
			$this->load->view('admin/_layout_main',$this->data);
		}
	}
//////////////////////////////////////////// Slider function////////////////////////////////////////
	public function slider()
	{
		$this->data['partialView'] = 'admin/pages/slider';
		$this->data['page'] = 'Slider';
		$this->data['icon'] = '<i class="fa fa-arrow-right"></i>';

		$this->data['sliders'] = $this->model_slider->get();

		if($_GET['action'] != NULL){
		$action = $_GET['action'];
		}else{
			$action = 'add';
		}
		if($_GET['slug'] != NULL){
		$slug = $_GET['slug'];
		}else{
			$slug = '';
		}
		if($action == 'edit'){
			$this->data['slider_form'] = 'edit-slider';
			if($slug != ''){
				$this->data['slider_detail'] = $this->model_slider->get_by(array('slug' => $slug));
			}
		}else{
			$this->data['slider_form'] = 'add-slider';
		}

		if(!empty($_POST) && $action == 'add')
		{
			$data =  array('title' => $this->input->post('title'),
					'category' => $this->input->post('category'),
					'slug' => url_title($this->input->post('slug')),
					'description' => $this->input->post('description'),
					'pic' => $this->input->post('pic'),
					'status' => $this->input->post('status')
				);
			$this->model_slider->save($data);
			$this->session->set_flashdata('response', "Slider Saved Successfully!!");
			redirect('admin/slider?action=add&slug=');
		}
		else if(!empty($_POST) && $action == 'edit' && $slug != '')
		{
			$data =  array('title' => $this->input->post('title'),
					'category' => $this->input->post('category'),
					'slug' => url_title($this->input->post('slug')),
					'description' => $this->input->post('description'),
					'pic' => $this->input->post('pic'),
					'status' => $this->input->post('status')
				);
				$parameter =$this->input->post('id');
			$this->model_slider->save($data,$parameter);
			$this->session->set_flashdata('response', "Slider updated Successfully!!");
			redirect('admin/slider?action=add&slug=');
		}		else if($action == 'delete' && $slug != '')
		{
			$this->model_slider->delete_by(array('slug' => $slug));
			$this->session->set_flashdata('response', "Slider deleted Successfully!!");
			redirect('admin/slider?action=add&slug=');

		}
		else
		{
			$this->load->view('admin/_layout_main',$this->data);
		}
	}

	//////////////////////////////////////////// gallery function////////////////////////////////////////
	public function gallery()
	{
		$this->data['partialView'] = 'admin/pages/gallery';
		$this->data['page'] = 'Gallery';
		$this->data['icon'] = '<i class="fa fa-image"></i>';
		$this->load->view('admin/_layout_main',$this->data);
	}

	//////////////////////////////////////////// contact function////////////////////////////////////////
	public function contact()
	{
		$this->data['partialView'] = 'admin/pages/contact';
		$this->data['page'] = 'Contact';
		$this->data['icon'] = '<i class="fa fa-phone"></i>';
		$this->load->view('admin/_layout_main',$this->data);
	}

	//////////////////////////////////////////// members function////////////////////////////////////////
	public function members()
	{
		$this->data['partialView'] = 'admin/pages/members';
		$this->data['page'] = 'Members';
		$this->data['icon'] = '<i class="fa fa-users"></i>';
		$this->load->view('admin/_layout_main',$this->data);
	}

	//////////////////////////////////////////// rules function////////////////////////////////////////
	public function rules()
	{
		$this->data['partialView'] = 'admin/pages/rules';
		$this->data['page'] = 'Rules';
		$this->data['icon'] = '<i class="fa fa-cubes"></i>';
		$this->load->view('admin/_layout_main',$this->data);
	}

	//////////////////////////////////////////// ECA function////////////////////////////////////////
	public function eca()
	{
		$this->data['partialView'] = 'admin/pages/eca';
		$this->data['page'] = 'ECA';
		$this->data['icon'] = '<i class="fa fa-paw"></i>';
		$this->load->view('admin/_layout_main',$this->data);
	}

	//////////////////////////////////////////// studentVoice function////////////////////////////////////////
	public function studentVoice()
	{
		$this->data['partialView'] = 'admin/pages/student';
		$this->data['page'] = 'Student voice';
		$this->data['icon'] = '<i class="fa fa-microphone"></i>';
		$this->load->view('admin/_layout_main',$this->data);
	}

	//////////////////////////////////////////// principalMessage function////////////////////////////////////////
	public function principalMessage()
	{
		$this->data['partialView'] = 'admin/pages/principal-message';
		$this->data['page'] = 'Principal message';
		$this->data['icon'] = '<i class="fa fa-envelope"></i>';
		$this->load->view('admin/_layout_main',$this->data);
	}

	//////////////////////////////////////////// migration function////////////////////////////////////////
	public function migration()
	{
		$this->load->library('migration');
		if ($this->migration->current() === FALSE)
		    {
		         show_error($this->migration->error_string());
		    }else{
		echo 'Migration Succeed !! WALAHAA';
		}
	}

	//////////////////////////////////////////// Message function////////////////////////////////////////
		public function Message()
		{
			$this->data['partialView'] = 'admin/pages/message';
			$this->data['page'] = 'Message';
			$this->data['icon'] = '<i class="fa fa-envelope"></i>';
			$this->load->view('admin/_layout_main',$this->data);
		}

		//////////////////////////////////////////// Notification function////////////////////////////////////////
		public function Notification()
		{
			$this->data['partialView'] = 'admin/pages/notification';
			$this->data['page'] = 'Notification';
			$this->data['icon'] = '<i class="fa fa-bell"></i>';
			$this->load->view('admin/_layout_main',$this->data);
		}

		//////////////////////////////////////////// Profile function////////////////////////////////////////
		public function Profile()
		{
			$this->data['partialView'] = 'admin/pages/profile';
			$this->data['page'] = 'Profile';
			$this->data['icon'] = '<i class="fa fa-user"></i>';
			$this->load->view('admin/_layout_main',$this->data);
		}

		//////////////////////////////////////////// PasswordGenerator function////////////////////////////////////////
		public function PasswordGenerator()
		{
			$this->data['partialView'] = 'admin/pages/password-generator';
			$this->data['page'] = 'Password Generator';
			$this->data['icon'] = '<i class="fa fa-key"></i>';
			$this->load->view('admin/_layout_main',$this->data);
		}

		//////////////////////////////////////////// Setting function////////////////////////////////////////
		public function Setting()
		{
			$this->data['partialView'] = 'admin/pages/setting';
			$this->data['page'] = 'Setting';
			$this->data['icon'] = '<i class="fa fa-cog"></i>';
			$this->load->view('admin/_layout_main',$this->data);
		}
	}
