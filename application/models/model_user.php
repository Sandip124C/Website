<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Model_user extends MY_Model{

  protected $_table_name='users';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';
	protected $_rules=array();
	protected $_timestamps=FALSE;

        function __construct(){
            parent::__construct(); //call the model constructor
        }

    }
?>
