<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Model_eca extends MY_Model{

  protected $_table_name='eca';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';
	protected $_order='ASC';
	public $_rules=array(
		'title' => array(
			'field' => 'title',
			'label' => 'Title',
			'rules' => 'trim|required|min_length[3]|max_length[100]'
		),
    'category' => array(
      'field' => 'category',
      'label' => 'Category',
      'rules' => 'trim|required|max_length[100]'
    ),
		'slug' => array(
			'field' => 'slug',
			'label' => 'Slug',
			'rules' => 'trim|required|min_length[3]|max_length[100]|callback__unique_slug_eca|url_title'
		),
		'description' => array(
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'trim|required'
		),
    'pic' => array(
      'field' => 'pic',
      'label' => 'Pic',
      'rules' => 'trim'
    ),
  );
	protected $_timestamps=TRUE;

        function __construct(){
            parent::__construct(); //call the model constructor
        }
      }
?>
