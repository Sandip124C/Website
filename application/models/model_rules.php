<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Model_rules extends MY_Model{

  protected $_table_name='rules';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';
	protected $_order='ASC';
	public $_rules=array();
	protected $_timestamps=TRUE;

        function __construct(){
            parent::__construct(); //call the model constructor
        }
      }
?>
