<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Model_notice extends MY_Model{

  protected $_table_name='notices';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';
	protected $_rules=array();
	protected $_timestamps=FALSE;

        function __construct(){
            parent::__construct(); //call the model constructor
        }

    }
?>
