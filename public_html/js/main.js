$(document).ready(function () {
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 40) {
            $('#header').addClass('navbar-fixed-top');
            $('#header').css('margin-top','-40px');
        } else if (scroll <= 40) {
            $('#header').removeClass('navbar-fixed-top');
            $('#header').css('margin-top', '0px');
        }
    });
});
